# yz858-ids721-mini9

This is a Python web project where a Hugging Face's LLM model is integrated and used. I used a container to manage the environment because I don't have pip in my local environment. Deploy it to streamlit!

## Requirements
- [x] Create a functioning website using Streamlit (25%)
- [x] Connect to an open source LLM (Hugging Face) (25%)
- [x] Documentation (20%)
- [x] Aesthetics (15%)
- [x] Chatbot performance (15%)
- [] Bonus: hosting EC2 or provider outside of Streamlit (15%)


## Steps
0. Model Selection

- https://huggingface.co/microsoft/Phi-3-mini-128k-instruct: light weight, but error downloading dependencies to run this model
- https://huggingface.co/meta-llama/Meta-Llama-3-8B: popular model, but needs large download so takes long long time to docker build and has access issue
- https://huggingface.co/docs/transformers/en/model_doc/gpt2 ✅


1. Use Docker to containerize the project
```
docker build -t python-streamlit . //Build the Docker image
docker run --rm -p 8501:8501 python-streamlit
```


## Screenshots

![Demo](mini9.gif)