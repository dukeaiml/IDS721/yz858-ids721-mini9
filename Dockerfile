# # Use an official Python runtime as a parent image
# FROM python:3.10

# # Set the working directory in the container
# WORKDIR /app

# # Copy the current directory contents into the container at /app
# COPY . /app

# # Install dependencies in a single RUN command to reduce layer size
# RUN pip install --upgrade pip && \
#     pip uninstall -y torch && \
#     pip cache purge && \
#     pip install torch -f https://download.pytorch.org/whl/torch_stable.html && \
#     pip install --no-cache-dir -r requirements.txt

# # Expose 8501 from the container
# EXPOSE 8501

# # Run app.py when the container launches
# CMD ["streamlit", "run", "app.py"]


# Use an official Python runtime as a parent image
FROM python:3.10

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install dependencies in a single RUN command to reduce layer size
RUN pip install --no-cache-dir -r requirements.txt

# Expose 8501 from the container
EXPOSE 8501

# Run app.py when the container launches
CMD ["streamlit", "run", "app.py"]
