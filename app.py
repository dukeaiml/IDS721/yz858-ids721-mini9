import streamlit as st
from streamlit_lottie import st_lottie
import requests
from transformers import pipeline

# Page Set Up
text_generator = pipeline("text-generation", model="gpt2")

st.set_page_config(page_title="Hugging Face Website", page_icon=":tada:", layout="wide")

def load_lottieurl(url):
    try:
        response = requests.get(url)
        response.raise_for_status()  # Raise an exception for HTTP errors
        return response.json()
    except requests.RequestException as e:
        print("Error fetching JSON data:", e)
        return None


# Load Assets
plant = load_lottieurl("https://lottie.host/245f2f60-f483-4da1-b2a5-98019b48e772/3zzHNaOhY5.json")

# Header Section
with st.container():
    left_column, right_column = st.columns(2)
    with left_column:
        st.header("This is where you can talk with Hugging Face's LLM model !")
        st.subheader("Come have a try!")
        st.write("The model in use here is openai-community/gpt2 for the text generation purpose.")
    with right_column:
        if plant:
            st_lottie(plant, height=300, key="plant")
    
st.write("---")

# Chatbot Section
# Function to capture the text input when Enter is pressed
def capture_text_input():
    st.write("User:", user_input)
    st.write("Model:", text_generator(user_input,max_length=100, do_sample=True))

    # model.getResponse(user_input)
    # for item in model.messages:
    #     st.write(item.get("role"), ":", item.get("content"))

# Create a text input widget with a label and a key
user_input = st.text_input("Enter your text:", "Hi there", key="text_input_key")

# Create a button widget
if st.button("Enter"):
    capture_text_input()